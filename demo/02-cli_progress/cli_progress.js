const { SingleBar, Presets } = require('cli-progress');

module.exports = new SingleBar({}, Presets.shades_classic);