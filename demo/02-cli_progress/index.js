const stream = require('../../index')
const terminalProgressBar = require('./cli_progress')

const start = () => {
  let total = 1000
  let i = 0
  
  const fileStream = stream('./results/test_02-cli_progress.txt')
  terminalProgressBar.start(total, 0);

  const createFile = () => {
    while(i < total) {
      fileStream.write(`${i+1} - asdasdsadads;asd;asd;asd;asd;asdasd;sad;sa;d;asd;as;d;asdas;d;a;d;asd;as;da;sd;a;sd;asd;asdas;da;sda;sd;as;dsa;d;asd;asdas;da;das;d;as;dasdasdsad;asd;\n`)
      i++
      terminalProgressBar.increment()
    }
  }

  createFile()

  terminalProgressBar.stop()

}

start()