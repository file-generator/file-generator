const stream01 = require('./stream01')
const stream02 = require('./stream02')

const start = () => {
  let total = 100
  
  const createFile1 = () => {
    let i = 0
    while(i < total) {
      stream01.write(`${i+1} - file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;\n`)
      i++
    }
  }

  const createFile2 = () => {
    let j = 0
    while(j < total) {
      stream02.write(`${j+1} - file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;\n`)
      j++
    }
  }

  createFile1()
  createFile2()
  
}

start()