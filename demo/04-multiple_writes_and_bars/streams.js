const stream = require('../../index')

module.exports = {
  stream01: stream('./results/test_04-multi-file-bars_01.txt'),
  stream02: stream('./results/test_04-multi-file-bars_02.txt'),
  stream03: stream('./results/test_04-multi-file-bars_03.txt'),
}