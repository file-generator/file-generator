const { MultiBar, Presets } = require('cli-progress');

module.exports = new MultiBar({
  format: ' {bar} | {file} | {value}/{total}', 
  hideCursor: true,
  clearOnComplete: false,
}, Presets.rect);