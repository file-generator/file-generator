const { stream01, stream02, stream03 } = require('./streams')

const terminalMultProgressBar = require('./cli_mult-progress')

const start = () => {
  let total = 5000
  const terminalProgressBar01 = terminalMultProgressBar.create(total, 0, { file: 'stream 01'});
  const terminalProgressBar02 = terminalMultProgressBar.create(total, 0, { file: 'stream 02'});
  const terminalProgressBar03 = terminalMultProgressBar.create(total, 0, { file: 'stream 03'});
    
  const createFile1 = () => {
    let i = 0
    while(i < total) {
      stream01.write(`${i+1} - file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;file 01;\n`)
      i++
      terminalProgressBar01.update(i)
      terminalMultProgressBar.update()
    }
  }
  
  const createFile2 = () => {
    let j = 0
    while(j < total) {
      stream02.write(`${j+1} - file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;file 02;\n`)
      stream03.write(`${(j+1).toString().padStart(2, '0')} - stream03;\n`)
      j++
      terminalProgressBar02.update(j)
      terminalProgressBar03.update(j)
      terminalMultProgressBar.update()

    }
  }

  createFile1()
  createFile2()

  terminalMultProgressBar.stop();

}

start()
