var fs = require('fs')

module.exports = (fileName, options) => {
  const outputWriteStream = fs.createWriteStream(fileName, options);
  outputWriteStream.setMaxListeners(0)

  return {
    ...outputWriteStream,
    write: (text) => 
    !outputWriteStream.write(text) &&
     outputWriteStream.once('drain', () => {})
  }
}